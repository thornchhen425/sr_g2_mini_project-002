import React from "react";
import Header from "./components/Header";
import { Container } from "react-bootstrap";
import FormPerson from "./components/FormPerson";
import DisplayData from "./components/DisplayData";
import ViewDetail from "./components/ViewDetail";

import "./App.css";

class PersonInformation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isEdit: false,
      isViewDetail: false,
      person: undefined,
      persons: [
        {
          id: 1,
          username: "Sam an Davit",
          gender: "Male",
          email: "ditway1122@gmail.com",
          jobs: { Student: true, Developer: true },
          createAt: Date.now(),
          updateAt: Date.now(),
        },
        {
          id: 2,
          username: "Thorn Chhen",
          gender: "Male",
          email: "thornchhen@gmail.com",
          jobs: { Student: true, Developer: true },
          createAt: Date.now(),
          updateAt: Date.now(),
        },
        {
          id: 3,
          username: "Sous Monika",
          gender: "Female",
          email: "monika@gmail.com",
          jobs: { Student: true, Developer: true },
          createAt: Date.now(),
          updateAt: Date.now(),
        },
        {
          id: 4,
          username: "Phanny Nayvannet",
          gender: "Male",
          email: "nayvannet@gmail.com",
          jobs: { Student: true, Developer: true },
          createAt: Date.now(),
          updateAt: Date.now(),
        },
        {
          id: 5,
          username: "Sngoun Leehou",
          gender: "Male",
          email: "leehou@gmail.com",
          jobs: { Student: true, Developer: true },
          createAt: Date.now(),
          updateAt: Date.now(),
        },
      ],
    };
  }
  onAction = (type, item) => {
    switch (type) {
      case "view":
        this.onViewDetail(item);
        break;
      case "edit":
        this.onEdit(item);
        break;
      case "delete":
        this.onDelete(item);
        break;
    }
  };
  genId = () => {
    const { persons } = this.state;
    if (persons.length == 0) return 1;
    return persons.map((item) => item.id).reduce((a, b) => Math.max(a, b)) + 1;
  };
  onEdit = (person) => {
    this.setState({ isEdit: true, person });
  };
  onCancelEdit = () => {
    this.setState({ isEdit: false });
  };
  onSubmit = (value) => {
    console.log(value);
    const { persons, isEdit } = this.state;
    if (isEdit) {
      this.setState({
        isEdit: false,
        persons: [
          ...persons.map((item) =>
            item.id === value.id ? { ...value, updateAt: Date.now() } : item
          ),
        ],
      });
    } else {
      this.setState({
        isEdit: false,
        persons: [...persons, { ...value, id: this.genId() }],
      });
    }
  };
  onDelete = (person) => {
    const { persons } = this.state;
    this.setState({
      persons: [...persons.filter((item) => item.id !== person.id)],
    });
  };
  onViewDetail = (person) => {
    this.setState({ person, isViewDetail: true });
  };
  onViewDetailClose = () => {
    this.setState({ isViewDetail: false });
  };

  render() {
    const { persons, isViewDetail, person, isEdit, currentPage } = this.state;
    return (
      <div>
        <Header />
        <Container>
          <FormPerson
            onSubmit={this.onSubmit}
            isEdit={isEdit}
            person={person}
            onCancel={this.onCancelEdit}
          />
          <DisplayData
            isEdit={isEdit}
            onAction={this.onAction}
            persons={persons}
          />
          {person && (
            <ViewDetail
              show={isViewDetail}
              person={person}
              onClose={this.onViewDetailClose}
            />
          )}
        </Container>
      </div>
    );
  }
}

export default PersonInformation;
