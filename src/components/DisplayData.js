import { useEffect, useState } from "react";
import {
  ButtonGroup,
  Table,
  Card,
  Row,
  Col,
  Dropdown,
  ToggleButton,
  Button,
} from "react-bootstrap";
import { MdVisibility, MdEdit, MdDelete } from "react-icons/md";
import JobList from "./JobList";
import Pagination from "./Pagination";
import Moment from "react-moment";

export default function (props) {
  const perPage = 3;
  const [listStyle, setListStyle] = useState("table");
  const { persons = [], isEdit, onAction } = props;
  const [currentPage, setCurrentPage] = useState(1);
  const [personList, setPersonList] = useState([]);

  useEffect(() => {
    renderLayout();
  }, [isEdit]);

  useEffect(() => {
    setPersonList(getPersonsByPage(currentPage));
  }, [persons]);

  function getTotalPage() {
    return Math.ceil(persons.length / perPage);
  }
  function onPageChange(page) {
    setCurrentPage(page);
    setPersonList(getPersonsByPage(page));
  }

  function getPersonsByPage(page) {
    const indexOfLast = page * perPage;
    const indexOfFirst = indexOfLast - perPage;
    return persons.slice(indexOfFirst, indexOfLast);
  }
  function onListChange(e) {
    const type = e.currentTarget.value;
    setListStyle(type);
  }
  function renderLayout() {
    switch (listStyle) {
      case "table":
        return tableStyle();
      case "card":
        return cardStyle();
    }
  }
  function cardStyle() {
    return <Row className="flex-wrap">{personList.map(renderCard)}</Row>;
  }
  function renderCard(item, index) {
    return (
      <Col className="col-3 mb-4" key={index}>
        <Card>
          <Card.Header className="d-flex justify-content-center">
            <Dropdown onSelect={(e) => onAction(e, item)}>
              <Dropdown.Toggle size="sm" variant="success">
                Action
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item disabled={isEdit} eventKey="view">
                  View
                </Dropdown.Item>
                <Dropdown.Item eventKey="edit">Edit</Dropdown.Item>
                <Dropdown.Item disabled={isEdit} eventKey="delete">
                  Delete
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Card.Header>
          <Card.Body>
            <h5>{item.username}</h5>
            <h6 className="m-0">Job</h6>
            <JobList jobs={item.jobs} />
          </Card.Body>
          <Card.Footer className="d-flex justify-content-center">
            <p className="m-0">
              <Moment date={item.updateAt} fromNow ago locale="km" />
            </p>
          </Card.Footer>
        </Card>
      </Col>
    );
  }
  function renderRow(item, index) {
    return (
      <tr key={index}>
        <td>{item.id}</td>
        <td>{item.username}</td>
        <td>{item.gender}</td>
        <td>{item.email}</td>
        <td>
          <JobList jobs={item.jobs} />
        </td>
        <td>
          <Moment date={item.createAt} fromNow ago locale="km" />
        </td>
        <td>
          <Moment date={item.updateAt} fromNow ago locale="km" />
        </td>
        <td>
          <div className="d-flex">
            <Button
              disabled={isEdit}
              className="d-flex align-items-center mr-2"
              size="sm"
              variant="primary"
              onClick={() => onAction("view", item)}
            >
              <MdVisibility className="mr-2" /> View
            </Button>
            <Button
              className="d-flex align-items-center mr-2"
              size="sm"
              variant="secondary"
              onClick={() => onAction("edit", item)}
            >
              <MdEdit className="mr-2" />
              Edit
            </Button>
            <Button
              disabled={isEdit}
              className="d-flex align-items-center"
              size="sm"
              variant="danger"
              onClick={() => onAction("delete", item)}
            >
              <MdDelete className="mr-2" />
              Delete
            </Button>
          </div>
        </td>
      </tr>
    );
  }
  function tableStyle() {
    return (
      <Table bordered>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Gender</th>
            <th>Email</th>
            <th>Job</th>
            <th>Create At</th>
            <th>Update At</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{personList.map(renderRow)}</tbody>
      </Table>
    );
  }
  return (
    <div>
      <h3 className="my-3">Display Data</h3>
      <ButtonGroup className="mb-3" size="sm" toggle>
        <ToggleButton
          variant="secondary"
          type="radio"
          onChange={onListChange}
          checked={listStyle === "table"}
          value="table"
        >
          Table
        </ToggleButton>

        <ToggleButton
          variant="secondary"
          type="radio"
          onChange={onListChange}
          checked={listStyle === "card"}
          value="card"
        >
          Card
        </ToggleButton>
      </ButtonGroup>
      {renderLayout()}
      <Pagination
        onClick={onPageChange}
        count={getTotalPage()}
        page={currentPage}
      />
    </div>
  );
}
