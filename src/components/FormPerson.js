import { useFormik } from "formik";
import { useEffect } from "react";
import { Col, Form, Row, Button } from "react-bootstrap";
import * as Yup from "yup";
export default function (props) {
  const { person, onSubmit, onCancel, isEdit } = props;
  const validationSchema = Yup.object().shape({
    username: Yup.string()
      .matches("^[a-zA-Z\\s]+$", "Username allow character only")
      .required("Username is required"),
    email: Yup.string().email("Email is invalid").required("Email is required"),
  });
  const {
    values,
    handleChange,
    handleSubmit,
    errors,
    setFieldValue,
    setValues,
  } = useFormik({
    validationSchema,
    onSubmit,
    initialValues: isEdit
      ? person
      : {
          username: "",
          email: "",
          gender: "Male",
          jobs: {
            Student: true,
          },
          createAt: Date.now(),
          updateAt: Date.now(),
        },
    validateOnChange: false,
  });
  useEffect(() => {
    setValues(
      isEdit
        ? person
        : {
            username: "",
            email: "",
            gender: "Male",
            jobs: {
              Student: true,
            },
            createAt: Date.now(),
            updateAt: Date.now(),
          }
    );
  }, [isEdit, person]);
  return (
    <div {...props}>
      <h1 className="font-weight-normal my-3">Person Info</h1>
      <Form onSubmit={handleSubmit}>
        <Row>
          <Col>
            <Form.Group>
              <Form.Label>Username</Form.Label>
              <Form.Control
                type="text"
                onChange={handleChange}
                value={values.username}
                placeholder="Enter username"
                name="username"
                isInvalid={!!errors.username}
              />
              <Form.Control.Feedback type="invalid">
                {errors.username}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="text"
                onChange={handleChange}
                value={values.email}
                placeholder="Enter email"
                name="email"
                isInvalid={!!errors.email}
              />
              <Form.Control.Feedback type="invalid">
                {errors.email}
              </Form.Control.Feedback>
            </Form.Group>
            <div className="d-flex justify-content-end">
              <Button variant="primary" type="submit">
                {isEdit ? "Save" : "Submit"}
              </Button>
              {isEdit && (
                <Button className="ml-3" variant="secondary" onClick={onCancel}>
                  Cancel
                </Button>
              )}
            </div>
          </Col>
          <Col>
            <h4>Gender</h4>
            <div className="d-flex mb-4">
              <Form.Check
                onChange={handleChange}
                checked={values.gender === "Male"}
                value="Male"
                label="Male"
                type="radio"
                name="gender"
                className="mr-3"
              />
              <Form.Check
                onChange={handleChange}
                checked={values.gender === "Female"}
                value="Female"
                label="Female"
                type="radio"
                name="gender"
              />
            </div>
            <h4>Job</h4>
            <div className="d-flex">
              <Form.Check
                onChange={(e) =>
                  setFieldValue("jobs.Student", e.currentTarget.checked)
                }
                label="Student"
                checked={values.jobs.Student}
                className="mr-3"
              />
              <Form.Check
                onChange={(e) =>
                  setFieldValue("jobs.Teacher", e.currentTarget.checked)
                }
                label="Teacher"
                checked={values.jobs.Teacher}
                className="mr-3"
              />
              <Form.Check
                onChange={(e) =>
                  setFieldValue("jobs.Developer", e.currentTarget.checked)
                }
                label="Developer"
                checked={values.jobs.Developer}
              />
            </div>
          </Col>
        </Row>
      </Form>
    </div>
  );
}
