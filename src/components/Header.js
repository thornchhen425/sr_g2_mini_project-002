import { Container } from "react-bootstrap";

export default function(props) {
    return <div {...props} className='header'>
        <Container className='d-flex p-3'>
           <img width={30} height={30} src="https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png"/>
           <h5 className='m-0 ml-3'>Person Info React Bootstrap</h5>
        </Container>
    </div>
}