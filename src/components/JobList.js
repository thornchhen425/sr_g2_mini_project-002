export default function (props) {
  const {jobs} = props;
  const jobKeys = Object.keys(jobs);
  return (
    <ul>
      {jobKeys.map((job, index) => {
        if (jobs[job]) {
          return <li key={index}>{job}</li>;
        }
        return undefined;
      })}
    </ul>
  );
}
