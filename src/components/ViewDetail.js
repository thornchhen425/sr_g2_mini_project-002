import { Col, Row, Modal, Button } from "react-bootstrap";
import JobList from "./JobList";

export default function (props) {
  const { person, onClose } = props;
  return (
    <Modal {...props} onHide={onClose} backdrop='static' centered>
      <Modal.Header closeButton>
        <Modal.Title>Information</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Row>
          <Col>
            <h4>{person.username}</h4>
            <h6>{person.email}</h6>
            <h5>{person.gender}</h5>
          </Col>
          <Col>
            <h4>Job</h4>
            <JobList jobs={person.jobs} />
          </Col>
        </Row>
      </Modal.Body>
      <Modal.Footer>
        <div className="d-flex justify-content-end">
          <Button variant="primary" onClick={onClose}>
            Close
          </Button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
