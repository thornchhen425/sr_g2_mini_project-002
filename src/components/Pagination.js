import { Pagination } from "react-bootstrap";

export default function (props) {
  const { page, count = 0, onClick } = props;
  function renderPage() {
    const pages = [];
    for (let i = 1; i < count + 1; i++) {
      pages.push(
        <Pagination.Item onClick={() => onClick(i)} key={i} active={i === page}>
          {i}
        </Pagination.Item>
      );
    }
    return pages;
  }
  return (
    <div className="d-flex justify-content-center">
      <Pagination>{renderPage()}</Pagination>
    </div>
  );
}
